namespace WebAPI.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Innovations : DbContext
    {
        public Innovations()
            : base("name=Innovations")
        {
        }

        public virtual DbSet<C__MigrationHistory> C__MigrationHistory { get; set; }
        public virtual DbSet<acceptanceCriteria> acceptanceCriterias { get; set; }
        public virtual DbSet<area> areas { get; set; }
        public virtual DbSet<AspNetRole> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaim> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogin> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUser> AspNetUsers { get; set; }
        public virtual DbSet<comment> comments { get; set; }
        public virtual DbSet<FollowingIdea> FollowingIdeas { get; set; }
        public virtual DbSet<idea> ideas { get; set; }
        public virtual DbSet<ideasArea> ideasAreas { get; set; }
        public virtual DbSet<vote> votes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<area>()
                .Property(e => e.areaName)
                .IsUnicode(false);

            modelBuilder.Entity<area>()
                .HasMany(e => e.ideasAreas)
                .WithRequired(e => e.area)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AspNetRole>()
                .HasMany(e => e.AspNetUsers)
                .WithMany(e => e.AspNetRoles)
                .Map(m => m.ToTable("AspNetUserRoles").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.acceptanceCriterias)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.userID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.AspNetUserClaims)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.AspNetUserLogins)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.comments)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.userID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.ideas)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.InitiatingUSERId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.votes)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.userID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.FollowingIdeas)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.userID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<idea>()
                .Property(e => e.frequency)
                .IsUnicode(false);

            modelBuilder.Entity<idea>()
                .Property(e => e.impact)
                .IsUnicode(false);

            modelBuilder.Entity<idea>()
                .HasMany(e => e.acceptanceCriterias)
                .WithRequired(e => e.idea)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<idea>()
                .HasMany(e => e.comments)
                .WithRequired(e => e.idea)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<idea>()
                .HasMany(e => e.FollowingIdeas)
                .WithRequired(e => e.idea)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<idea>()
                .HasMany(e => e.ideasAreas)
                .WithRequired(e => e.idea)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<idea>()
                .HasMany(e => e.votes)
                .WithRequired(e => e.idea)
                .WillCascadeOnDelete(false);
        }
    }
}

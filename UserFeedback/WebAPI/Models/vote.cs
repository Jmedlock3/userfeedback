namespace WebAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class vote
    {
        public int voteID { get; set; }

        [Column("vote")]
        public bool vote1 { get; set; }

        [Required]
        [StringLength(128)]
        public string userID { get; set; }

        public int ideaID { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }

        public virtual idea idea { get; set; }
    }
}

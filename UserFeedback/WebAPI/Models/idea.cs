namespace WebAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class idea
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public idea()
        {
            acceptanceCriterias = new HashSet<acceptanceCriteria>();
            comments = new HashSet<comment>();
            FollowingIdeas = new HashSet<FollowingIdea>();
            ideasAreas = new HashSet<ideasArea>();
            votes = new HashSet<vote>();
        }

        public int ideaID { get; set; }

        [Required]
        [StringLength(500)]
        public string releaseNote { get; set; }

        [Required]
        [StringLength(128)]
        public string InitiatingUSERId { get; set; }

        [StringLength(500)]
        public string jiraURL { get; set; }

        [StringLength(1000)]
        public string userStory { get; set; }

        public DateTime createdDate { get; set; }

        public DateTime? modifiedDate { get; set; }

        public DateTime? archiveDate { get; set; }

        public bool? newFeature { get; set; }

        [StringLength(30)]
        public string frequency { get; set; }

        [StringLength(30)]
        public string impact { get; set; }

        public bool? requiresTraining { get; set; }

        public bool? customerFacing { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<acceptanceCriteria> acceptanceCriterias { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<comment> comments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FollowingIdea> FollowingIdeas { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ideasArea> ideasAreas { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<vote> votes { get; set; }
    }
}

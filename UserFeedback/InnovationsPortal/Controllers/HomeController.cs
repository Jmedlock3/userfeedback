﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InnovationsPortal.Models;
using System.Net;
using Microsoft.AspNet.Identity;

namespace InnovationsPortal.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private Innovations db = new Innovations();
        public ActionResult Index()
        {
            string currentUser = User.Identity.GetUserId();
            DashboardViewModels dbm = new DashboardViewModels();
            dbm.MyIdeas = (from d in db.ideas
                          join u in db.AspNetUsers on d.InitiatingUSERId equals u.Id 
                          where d.InitiatingUSERId == currentUser
                          select d).ToList();
           
            dbm.FollowedIdeas = (from d in db.ideas
                                 join u in db.FollowingIdeas on d.ideaID equals u.ideaID
                                 join i in db.AspNetUsers on u.userID equals i.Id
                                 where u.userID == currentUser
                                 select d).ToList();
            //ViewData.Model = dbm;
         
            return View(dbm);
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LoadCreator([Bind(Include = "InitiatingUSERId ,releaseNote, userStory, requiresTraining, newFeature, customerFacing")] idea idea)
        {
            idea.InitiatingUSERId = User.Identity.GetUserId();
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            
            if (ModelState.IsValid)
            {
                //should be authenticated user ID
                idea.createdDate = DateTime.Now.ToUniversalTime();
                db.ideas.Add(idea);
                db.SaveChanges();
                int id = idea.ideaID;
                return RedirectToAction("Idea/" + id);
            }
            return View(idea);
        }
        public ActionResult Idea(int? ID)
        {
            if (ID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            idea idea = db.ideas.Find(ID);
            
            if (idea == null)
            {
                return HttpNotFound();
            }
            
            return View(idea);
        }
        public ActionResult LoadCreator()
        {
            return PartialView("_PartialCreate");
        }
        public ActionResult UserVote(int ideaID)
        {
            string currentUser = User.Identity.GetUserId();
            vote uservote = (from v in db.votes where v.userID == currentUser && v.ideaID == ideaID select v).FirstOrDefault();
            if (uservote == null)
            {
                uservote = new vote();
                uservote.ideaID = ideaID;
            }
            return PartialView("_PartialUserVote", uservote);
        }
        public JsonResult AddAcceptanceCriteria(acceptanceCriteria ac)
        {
            ac.createdDate = DateTime.Now.ToUniversalTime();
            ac.userID = User.Identity.GetUserId();
            db.acceptanceCriterias.Add(ac);
            return Json(db.SaveChanges(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult AddComment(comment cmt)
        {
            cmt.createdDate = DateTime.Now.ToUniversalTime();
            cmt.userID = User.Identity.GetUserId();
            db.comments.Add(cmt);
            return Json(db.SaveChanges(), JsonRequestBehavior.AllowGet);
        }
        //public JsonResult AddVote(vote newVote)
        //{
        //    newVote.userID = User.Identity.GetUserId();
        //    db.votes.Add(newVote);
        //    db.SaveChanges();
        //    return PartialView("UserVote", newVote.ideaID);
        //    //return Json(db.SaveChanges(), JsonRequestBehavior.AllowGet);
        //    //return Json(new { Url = Url.Action("UserVote","Home", newVote.ideaID), JsonRequestBehavior.DenyGet });
        //}
        [HttpPost]
        public ActionResult AddVote(vote newVote)
        {
            newVote.userID = User.Identity.GetUserId();
            db.votes.Add(newVote);
            db.SaveChanges();
            if (Request.IsAjaxRequest())
            {
                return PartialView("_PartialUserVote", newVote);
            }
            return Json(new { Url = Url.Action("idea", newVote.ideaID), JsonRequestBehavior.DenyGet });
        }
        public JsonResult UpDateVote(vote currentVote)
        {
            db.Entry(currentVote).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            //return Json(currentVote, JsonRequestBehavior.AllowGet);
            return Json(new { Url = Url.Action("idea", new { id = currentVote.ideaID }), JsonRequestBehavior.DenyGet });
        }
        //[HttpPost]

        //public ActionResult UpDateVote(vote currentVote)
        //{
        //    db.Entry(currentVote).State = System.Data.Entity.EntityState.Modified;
        //    db.SaveChanges();
        //    if (Request.IsAjaxRequest())
        //    {
        //        return PartialView("_PartialUserVote", currentVote);
        //    }
        //    return Json(new { Url = Url.Action("UserVote", currentVote.ideaID),JsonRequestBehavior.DenyGet });
        //}
        public JsonResult UpdateIdea(idea updatedIdea)
        {
            updatedIdea.modifiedDate = DateTime.Now.ToUniversalTime();
            
            db.Entry(updatedIdea).State = System.Data.Entity.EntityState.Modified;
            return Json(db.SaveChanges(), JsonRequestBehavior.DenyGet );
        }
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
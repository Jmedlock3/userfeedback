﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InnovationsPortal.Models
{
    public class IdeaViewModels
    {
        
    }
    public class SingleIdea
    {
        private Innovations db = new Innovations();
        public idea idea { get; set; }
        public List<AspNetUser> AspNetUsers { get; set; }

        public SingleIdea()
        {

        }
        public SingleIdea(int? ideaID)
        {
            if (ideaID != null)
            {
                this.idea = db.ideas.Find(ideaID);
                this.AspNetUsers = (from a in db.AspNetUsers
                                    join i in db.ideas on a.Id equals i.InitiatingUSERId
                                    join c in db.comments on i.ideaID equals c.ideaID
                                    select a).ToList();
            }
        }
    }
}
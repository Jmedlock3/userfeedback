﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InnovationsPortal.Models
{
    [Table("FollowingIdeas")]
    public class FollowingIdeas
    {
        [Key]
        public int followID { get; set; }
        public int ideaID { get; set; }
        public string userID { get; set; }
        public virtual idea idea { get; set; }
    }
}
namespace InnovationsPortal.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ideasArea
    {
        public int ideasAreaID { get; set; }

        public int areaID { get; set; }

        public int ideaID { get; set; }

        public virtual area area { get; set; }

        public virtual idea idea { get; set; }
    }
}

namespace InnovationsPortal.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("acceptanceCriteria")]
    public partial class acceptanceCriteria
    {
        public int acceptanceCriteriaID { get; set; }

        [Column("acceptanceCriteria")]
        [Required]
        [StringLength(500)]
        public string acceptanceCriteria1 { get; set; }

        public int ideaID { get; set; }

        [Required]
        [StringLength(128)]
        public string userID { get; set; }

        public DateTime createdDate { get; set; }

        public virtual idea idea { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InnovationsPortal.Models
{
    public class DashboardViewModels
    {
        public List<idea> MyIdeas { get; set; }
        public List<idea> FollowedIdeas { get; set; }
        public List<vote> IdeaVotes { get; set; }

    }
}
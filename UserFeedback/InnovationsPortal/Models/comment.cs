namespace InnovationsPortal.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class comment
    {
        public int commentID { get; set; }

        [Column("comment")]
        [Required]
        [StringLength(500)]
        public string comment1 { get; set; }

        [Required]
        [StringLength(128)]
        public string userID { get; set; }

        public int ideaID { get; set; }

        public DateTime createdDate { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }

        public virtual idea idea { get; set; }
    }
}

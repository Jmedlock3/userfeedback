﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(InnovationsPortal.Startup))]
namespace InnovationsPortal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

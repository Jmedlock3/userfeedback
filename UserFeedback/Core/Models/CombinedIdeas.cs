﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Core.Models
{
    public class CombinedIdeas
    {
        public idea Idea { get; set; }
        public acceptanceCriteria AccepatanceCriteria { get; set; }
        public comment Comments { get; set; }
        public area Areas { get; set; }
        public ideasArea IdeasArea { get; set; }
    }
}
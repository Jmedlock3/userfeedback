﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Core.Models
{
    public class ideaModel
    {
        public int ideaID { get; set; }
        public string releaseNote { get; set; }
        public List<areas> areas { get; set; }
        public string jiraURL { get; set; }
        public string userStory { get; set; }
        public DateTime createdDate { get; set; }
        public DateTime modifiedDate { get; set; }
        public DateTime archiveDate { get; set; }
        public bool newFeature { get; set; }
        public string frequency { get; set; }
        public string impact { get; set; }
        public bool requiresTraining { get; set; }
        public bool customerFacing { get; set; }
        public List<AcceptanceCriteria> criteriaList { get; set; }
        public List<votes> voteList { get; set; }
        public List<comments> CommentList { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Core.Models
{
    public class comments
    {
        public int commentID { get; set; }
        public string comment { get; set; }
        public int userID { get; set; }
        public int ideaID { get; set; }
    }
}
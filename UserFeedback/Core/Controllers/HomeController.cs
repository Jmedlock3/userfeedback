﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Models;
using System.Net;
namespace Core.Controllers
{
    public class HomeController : Controller
    {
        private InnovationsEntities db = new InnovationsEntities();
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }
        public ActionResult IdeaView()
        {
            ViewBag.Title = "Inovation from Anywhere";
            bool authED = IdeaAuth.JiraAuthenticatedAPI("", "");
            return View();
        }
        public ActionResult DashboardView()
        {
            ViewBag.Title = "Let's Innovate";
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LoadCreator([Bind(Include = "releaseNote, userStory, newFeature, customerFacing")] idea idea)
        {
            if (ModelState.IsValid)
            {
                idea.InitiatingUSERId = 1;//should be authenticated user ID
                idea.createdDate = DateTime.Now.ToUniversalTime();
                db.ideas.Add(idea);
                db.SaveChanges();
                int id = idea.ideaID;
                return RedirectToAction("ExistingIdea/" + id);
            }
            return View(idea);
}
        public ActionResult LoadCreator()
        {
            return PartialView("_PartialCreate");
        }
        public ActionResult ExistingIdea(int? ID)
        {
            if (ID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            idea idea = db.ideas.Find(ID);
            //IQueryable<idea> idea2 = from d in db.ideas
            //                         join e in db.acceptanceCriterias on d.ideaID equals e.ideaID into de
            //                         from acpt in de.DefaultIfEmpty()
            //                         where d.ideaID == ID
            //                         select d;
            if (idea == null)
            {
                return HttpNotFound();
            }
            //idea existingIdea = new idea { releaseNote = "New Note" };
            return View(idea);
        }
        public JsonResult AddAcceptanceCriteria(acceptanceCriteria ac)
        {
            ac.createdDate = DateTime.Now.ToUniversalTime();
            db.acceptanceCriterias.Add(ac);
            return Json(db.SaveChanges(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult AddComment(comment cmt)
        {
            cmt.createdDate = DateTime.Now.ToUniversalTime();
            db.comments.Add(cmt);
            return Json(db.SaveChanges(), JsonRequestBehavior.AllowGet);
        }
    }
}
